import sys, logging, requests, time, math

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

#Get token bot
bit_token = sys.argv[1]

#variable Restarant 
RESTAU_CHOIX, RESTAU_RESULTATS, R_EXIT, RESTAU_RETOUR_CHOIX = range(4)

#current restaurent type
restauCurrentType = ''
restSelect = ''
restExit = ''

#variable Sortie 
S_TYPE, S_CHOIX, S_EXIT = range(3)


#START
def start(update, context):
    reply_keyboard = [['Restaurant','Sortie','Transport']]
    update.message.reply_text(
        'Salutation Humain !\n'
        'Je suis ici pour t\'aider à trouver des activités captivantes.\n'
        'Pour chercher un bon restaurent tape /restaurant\n'
        'Pour trouver des activités en pleine air, tape /sortie\n'
        'Envoyez /cancel pour couper toute communications avec moi.\n',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))    


def cancel(update, context):
    update.message.reply_text(
        'D\'accord, je coupe la communication. N\'hésite pas à taper /start \n'
        'Pour recommencer à parler avec moi.')

    return ConversationHandler.END



#Choix du Restaurant 
def restaurant(update, context):
    reply_keyboard = [['Italien'], ['Japonais'], ['Français'],['Retour']]

    update.message.reply_text(
        'Choisis ce que tu préfères manger.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )
    return RESTAU_CHOIX


#Choix du restaurant
def restaurant_type(update, context):
    global restauCurrentType

    selection = update.message.text

    if selection == 'Italien':
        reply_keyboard = [['Molino'], ['Vapiano'], ['Les Ormeaux'],['Changer de catégorie']]
        restType = 'Italien'

    elif selection == 'Japonais':
        reply_keyboard = [['Miyako'], ['Ukiyo'], ['Musuji'],['Changer de catégorie']]
        restType = 'Asiatique'

    elif selection == 'Français':
        reply_keyboard = [['Le Café Basque'], ['Pavillon Versoix'], ['Chez ma cousine'],['Changer de catégorie']]
        restType = 'Français'

    else :
        #L'utilisateur est revenu en arrière
        print('Comming from next')
        if restType == 'Italien':
            reply_keyboard = [['Molino'], ['Vapiano'], ['Les Ormeaux '],['Changer de catégorie']]

        elif restType == 'Japonais':
            reply_keyboard = [['Miyako'], ['Ukiyo'], ['Misuji'],['Changer de catégorie']]

        elif restType == 'Français':
            reply_keyboard = [['Le Café Basque'], ['Pavillon Versoix'], ['Chez ma cousine'],['Changer de catégorie']]

        else :
            return ConversationHandler.END


    update.message.reply_text(
        'Sélectionnez un restaurant',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return RESTAU_RESULTATS



#Affichage du restaurant sélectionné
def restaurant_choix(update, context):
    global restSelect
    restSelect = update.message.text
    reply_keyboard = [['Terminer'],['Revenir au resultats']]

    tabDescrib = {
        'Molino':'Si tu veux manger prêt du lac.\nhttps://molino.ch/fr/pizzeria-ristorante-molino-molard',
        'Vapiano':'Pour des pâtes bien fraîches.\nhttps://chfr.vapiano.com/fr/home/',
        'Les Ormeaux':'Pizzeria au Petit-Lancy.\nhttps://www.pizzeria-les-ormeaux.ch',
        'Miyako':'Pour de la bonne nourriture et du spectacle.\nhttp://miyako.ch',
        'Ukiyo':'Restaurant de nouille traditionnelle.\nhttps://www.ukiyo-noodlebar.com/',
        'Misuji':'Restaurant de sushi.\nhttp://misuji.ch/misuji/',
        'Le Café Basque':'Saveurs Basques raffinés.\nhttps://www.smood.ch/fr/livraison/restaurants/geneve/le-cafe-basque\n',
        'Pavillon Versoix':'si vous cherchez un large choix de tartar.\nhttps://www.smood.ch/fr/livraison/restaurants/geneve/pavillon-versoix',
        'Cafe du Grutli':'On est pas mieux que chez ma cousine !.\nhttp://www.chez-macousine-gauche.ch/?gclid=Cj0KCQjw1Iv0BRDaARIsAGTWD1vyxIyJs2jUyLMv-suwRT8vzBLpxWRNdtoXA4jjNeUdx78AF8L1U0MaArl4EALw_wcB&gclsrc=aw.ds'
    }

    if tabDescrib[restSelect] == None:
        msg = 'Oups, il y a eus un problème avec ce restaurent'
    else :
        msg = tabDescrib[restSelect]
        
        


    update.message.reply_text(
        msg,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return R_RETURN_TYPE



#Sortie selection catégorie
def sortie(update,context):
    reply_keyboard = [['Musée'],['Bar'],['Club'],['Restaurant'],['Retour']]
    update.message.reply_text(
        'Que veux-tu faire ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return S_TYPE



#Choix de la sortie
def sortie_liste(update,context):
    typeSortie = update.message.text

    if typeSortie == 'Musée':
        reply_keyboard = [['Musée de la croix rouge'], ['Musée d\'art et d\'histoires'], ['Musée d\'histoire naturelle'],['Retour']]

    elif typeSortie == 'Bar':
        reply_keyboard = [['Le Chat noir'], ['Le Kraken'], ['Le Manhattan'],['Retour']]

    elif typeSortie == 'Club':
        reply_keyboard = [['Java'], ['L\'usine'], ['Le village du soir'],['Retour']]

    else :
        return ConversationHandler.END

    update.message.reply_text(
        'Voici la slélection :',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return S_CHOIX


#Affichage du bar sélectionné
def sortie_choix(update,context):
    reply_keyboard = [['Super merci !'],['T\'as pas autre chose ?']]
    location_selected = update.message.text

    # Liste de toutes les sorties avec leur description
    tab = {
        'Le Chat Noir':'Une très bonne ambience et de la musique très forte',
        'Le Kraken':'Toujours fréquenté Et il y a une bonne raison à cela !',
        'Le Manhattan':'Ajouter une description',
        'Java':'Ajouter une description.',
        'L\'usine':'Ajouter une description.',
        'Le village du soir':'Ajouter une description.',
        'Musée de la croix rouge':'Ajouter une description.',
        'Musée d\'art et d\'histoires':'Ajouter une description.',
        'Musée d\'histoire naturelle':'Ajouter une descripter.'
    }


    if tab[location_selected] == None:
        msg = 'Ce choix n\'est pas disponible !'
    else :
        msg = tab[location_selected]

    update.message.reply_text(
        msg,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return S_EXIT




def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(bit_token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))

    # Add conversation handler
    restaurant_handler = ConversationHandler(
        entry_points=[CommandHandler('restaurant', restaurant),MessageHandler(Filters.regex('^(Restaurant)$'), restaurant)],

        states={
            RESTAU_CHOIX: [MessageHandler(Filters.regex('^(Italien|Asiatique|Français)$'), restaurant_type),MessageHandler(Filters.regex('^(Anuler)$'),cancel)],

            RESTAU_RETOUR_CHOIX: [MessageHandler(Filters.regex('^(Retour)$'),restaurant_type),MessageHandler(Filters.regex('^(Aurevoir)$'),cancel)],

            RESTAU_RESULTATS: [MessageHandler(Filters.regex('^(Changer de catégorie)$'), restaurant), MessageHandler(Filters.text, restaurant_choix)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    sortie_handler = ConversationHandler(
        entry_points=[CommandHandler('sortie', sortie), MessageHandler(Filters.regex('^(Sortie)$'), sortie)],

        states={
            S_TYPE: [MessageHandler(Filters.regex('^(Musée|Bar|Club)$'), sortie_liste), MessageHandler(Filters.regex('^(Retour)$'),cancel)],
            S_CHOIX: [MessageHandler(Filters.regex('^(Retour)$'), sortie),MessageHandler(Filters.text, sortie_choix)],
            S_EXIT: [MessageHandler(Filters.regex('^(Retour)$'), sortie),MessageHandler(Filters.regex('^(Aurevoir)$'),cancel)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )



    dp.add_handler(restaurant_handler)
    dp.add_handler(sortie_handler)
    

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


main()